This module provides integration between the Yun Pian SMS service and the SMS framework project.

You can now easily send message via SMS to user with china mobile using Yun Pian service.

Why we build the module:

There are a lot of SMS vendors, we have tried several. They really have a good API and their custom module.

However because we want to ensure sending the message to the user within 30 seconds that most of them are with china mobile, there is no such a good module and server helping us.

Yun Pian is a china local vendor, it's fast and has a good API.

So we suggest Yun Pian


Details can be found on the website http://www.yunpian.com.

API for SMS can refer the url http://www.yunpian.com/api/sms.html.

Ensure the smsframework module was installed and enabled

Configuration can be found at admin/smsframework/gateways/sms_yp

You can hava a quick test after you finished the configuration at admin/smsframework/devel, before that you should ensure SMS Devel was enabled.
